package main

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"os"
	"os/exec"
	"regexp"
	"strings"
)

var idCounter uint64
var idMap = make(map[string]string)

func idFor(origID string) string {
	id, ok := idMap[origID]
	if !ok {
		if origID == "" {
			id = ""
		} else {
			id = fmt.Sprintf("%d", idCounter)
		}
		idCounter++
		idMap[origID] = id
	}
	return id
}

var regexpRecvSnowflake = regexp.MustCompile(`^Received snowflake:\s*([\w/+]*)`)
var regexpProxyNoClient = regexp.MustCompile(`^Proxy ([\w/+]*) did not receive a Client offer.`)
var regexpDate = regexp.MustCompile(`^(....)/(..)/(..) ..:..:..$`)

func reformatTimestamp(timestamp string) (string, error) {
	m := regexpDate.FindStringSubmatch(timestamp)
	if m == nil {
		return "", fmt.Errorf("bad timestamp")
	}
	return m[1] + "-" + m[2] + "-" + m[3] + " 12:00:00", nil
}

type errLine struct {
	LineNo uint64
	Line   string
	Inner  error
}

func (err *errLine) Error() string {
	return fmt.Sprintf("%d: %v: %+q", err.LineNo, err.Inner, err.Line)
}

func xzReader(r io.Reader) (io.ReadCloser, error) {
	cmd := exec.Command("xz", "-d", "-c")
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}
	cmd.Stdin = r
	return stdout, cmd.Start()
}

func readerMagic(f *os.File) (io.ReadCloser, error) {
	pos, err := f.Seek(0, io.SeekCurrent)
	if err != nil {
		return nil, err
	}
	var buf [6]byte
	_, err = io.ReadFull(f, buf[:])
	if err != nil {
		return nil, err
	}
	// seek back to original position
	_, err = f.Seek(pos, io.SeekStart)
	if err != nil {
		return nil, err
	}
	switch {
	case bytes.Equal(buf[:], []byte{0xfd, 0x37, 0x7a, 0x58, 0x5a, 0x00}):
		return xzReader(f)
	default:
		return f, nil
	}
}

func openMagic(filename string) (io.ReadCloser, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	return readerMagic(f)
}

func processLine(line string, w *csv.Writer) error {
	if line[19] != ' ' {
		fmt.Fprintf(os.Stderr, "format error: %+q\n", line)
		return nil
	}
	logTimestamp, msg := line[:19], line[20:]
	timestamp, err := reformatTimestamp(logTimestamp)
	if err != nil {
		fmt.Fprintf(os.Stderr, "bad timestamp: %+q\n", logTimestamp)
		return nil
	}
	if strings.HasPrefix(msg, "ACME hostnames: ") {
		err = w.Write([]string{timestamp, "start", "", "", ""})
	} else if strings.HasPrefix(msg, "http: TLS handshake error ") {
		err = w.Write([]string{timestamp, "error", "", "", "tls"})
	} else if strings.HasPrefix(msg, "http2: server: error ") {
		err = w.Write([]string{timestamp, "error", "", "", "http2"})
	} else if strings.HasPrefix(msg, "http2: received GOAWAY ") {
		err = w.Write([]string{timestamp, "error", "", "", "http2"})
	} else if msg == "Starting HTTP-01 listener" {
	} else if msg == "Invalid data." {
		err = w.Write([]string{timestamp, "error", "", "", "invalid-data"})
	} else if msg == "Mismatched IDs!" {
		err = w.Write([]string{timestamp, "proxy-gets-none", "", "", "mismatched-ids"})
	} else if msg == "Passing client offer to snowflake proxy." {
		err = w.Write([]string{timestamp, "client-offers", "", "", ""})
	} else if msg == "Client: No snowflake proxies available." {
		err = w.Write([]string{timestamp, "client-gets-none", "", "", "no-proxies"})
	} else if msg == "Client: Timed out." {
		err = w.Write([]string{timestamp, "client-gets-none", "", "", "timeout"})
	} else if matches := regexpRecvSnowflake.FindStringSubmatch(msg); matches != nil {
		err = w.Write([]string{timestamp, "proxy-polls", idFor(matches[1]), "", ""})
	} else if matches := regexpProxyNoClient.FindStringSubmatch(msg); matches != nil {
		err = w.Write([]string{timestamp, "proxy-gets-none", idFor(matches[1]), "", "no-clients"})
	} else if msg == "Passing client offer to snowflake." {
		err = w.Write([]string{timestamp, "proxy-gets-offer", "", "", ""})
	} else if strings.HasPrefix(msg, "Received answer: ") || msg == "Received answer." {
		err = w.Write([]string{timestamp, "proxy-answers", "", "", ""})
	} else if msg == "Client: Retrieving answer" {
		err = w.Write([]string{timestamp, "client-gets-answer", "", "", ""})
	} else {
		return fmt.Errorf("cannot parse line")
	}
	return err
}

func process(f io.Reader, w *csv.Writer) error {
	var lineNo uint64
	s := bufio.NewScanner(f)
	for s.Scan() {
		lineNo++
		line := s.Text()
		err := processLine(line, w)
		if err != nil {
			return &errLine{LineNo: lineNo, Line: line, Inner: err}
		}
	}
	return s.Err()
}

func processFile(filename string, w *csv.Writer) error {
	f, err := openMagic(filename)
	if err != nil {
		return err
	}
	defer f.Close()
	return process(f, w)
}

func main() {
	flag.Parse()

	w := csv.NewWriter(os.Stdout)
	err := w.Write([]string{"timestamp", "event", "proxyid", "clientid", "additional"})
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
	for _, filename := range flag.Args() {
		err := processFile(filename, w)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s: %v\n", filename, err)
			os.Exit(1)
		}
	}
	w.Flush()
	err = w.Error()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
}
